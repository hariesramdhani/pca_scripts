#!/bin/bash
# This should take the data from hpc/pipeline and modify files and place them under required directory structure on master server
#code to take from goldenorb and put into test for goldenorb RNASeq
#code to take local microarrray given to me by Tyrone and send it to test

ds_id="$1"
data_type="$2" # m for Microarray, r for RNASeq, l for local microarray

if [ $# -eq 0 ]; then
    echo "have to provide a ds_id "
    exit 1
fi

if [ -z ${data_type} ]; then
    data_type="r" # for RNASeq
fi
if [ ! -d /tmp/pca ]; then
  mkdir /tmp/pca/
fi
mkdir /tmp/pca/${ds_id}

ssh isha@test.stemformatics.org "rm -fR /var/www/pylons-data/prod/PCAFiles/${ds_id} ;"
ssh isha@test.stemformatics.org "mkdir /var/www/pylons-data/prod/PCAFiles/${ds_id} ;"

if [ "$data_type" = "r" ]; then

    main_qc_dir=/short/yf0/stemformatics/data/datasets/${ds_id}/source/processed/qc
    targets_dir=/short/yf0/stemformatics/data/datasets/${ds_id}/source/raw

    scp in0190@raijin.nci.org.au:/${targets_dir}/targets.txt /tmp/pca/${ds_id}

    for name in CPM_log2 RPKM_log2 non_normalized TMM_RPKM_log2
    do

        scp in0190@raijin.nci.org.au:${main_qc_dir}/PCA_gene_expression_${name}_sample_data.tsv /tmp/pca/${ds_id}
        scp in0190@raijin.nci.org.au:${main_qc_dir}/PCA_gene_expression_${name}_screetable_prcomp_variance.tsv /tmp/pca/${ds_id}


        cd /tmp/pca/${ds_id}

        cp  PCA_gene_expression_${name}_sample_data.tsv pca.tsv
        cp  PCA_gene_expression_${name}_screetable_prcomp_variance.tsv screetable.tsv
        cp  targets.txt metadata.tsv

        if `head -1 pca.tsv | grep -q SampleID`; then
            echo "SampleID found. Make no changes."
        else
            sed -i ' 1 s/^/SampleID\t/' pca.tsv
        fi


        var="prcomp\teigenvalue"
        sed -i "1s/.*/$var/" screetable.tsv

        ssh isha@test.stemformatics.org "mkdir /var/www/pylons-data/prod/PCAFiles/${ds_id}/${name} "
        scp pca.tsv isha@test.stemformatics.org:/var/www/pylons-data/prod/PCAFiles/${ds_id}/${name}/
        scp metadata.tsv isha@test.stemformatics.org:/var/www/pylons-data/prod/PCAFiles/${ds_id}/${name}/
        scp screetable.tsv isha@test.stemformatics.org:/var/www/pylons-data/prod/PCAFiles/${ds_id}/${name}/

    done;
fi


if [ "$data_type" == "m" ]; then

    cd /tmp/pca/${ds_id}

    main_qc_dir=/data/datasets/${ds_id}/source/normalized

    for file in `ssh isha@pipeline.stemformatics.org "ls /data/datasets/${ds_id}/sample_pheno.* | grep -v sorted"`
    do
        scp isha@pipeline.stemformatics.org:${file} /tmp/pca/${ds_id}/
    done
    cat /tmp/pca/${ds_id}/sample_pheno.txt | sort > /tmp/pca/${ds_id}/${ds_id}_metadata.tsv

    header="SampleID\tSampleType"
    for file in `ls /tmp/pca/${ds_id}/sample_pheno.txt.* | grep -v template | grep -v sorted`
    do

        header="${header}\t${file##*.}"

        sort $file > ${file}.sorted
        join -1 1 -2 1  /tmp/pca/${ds_id}/${ds_id}_metadata.tsv ${file}.sorted -t $'\t' > /tmp/pca/${ds_id}/metadata.tsv.1
        cat /tmp/pca/${ds_id}/metadata.tsv.1 > /tmp/pca/${ds_id}/${ds_id}_metadata.tsv

    done;

    sed -i "1s/^/${header}\n/" /tmp/pca/${ds_id}/${ds_id}_metadata.tsv
    for norm_type in prenorm_pca postnorm_pca
    do
        for name in nolabels
        do
            scp isha@pipeline.stemformatics.org:/${main_qc_dir}/${norm_type}_${name}_sample_data.tsv /tmp/pca/${ds_id}/${ds_id}_${norm_type}_pca.tsv
            scp isha@pipeline.stemformatics.org:/${main_qc_dir}/${norm_type}_${name}_screetable_prcomp_variance.tsv /tmp/pca/${ds_id}/${ds_id}_${norm_type}_screetable.tsv


            var="prcomp\teigenvalue"
            sed -i "1s/.*/$var/" ${ds_id}_${norm_type}_screetable.tsv

            sed -i 's/\.CEL//g' ${ds_id}_${norm_type}_pca.tsv
            len=${#norm_type}
            ssh isha@test.stemformatics.org "rm -fR /var/www/pylons-data/prod/PCAFiles/${ds_id}/${norm_type:0:len-4}"
            ssh isha@test.stemformatics.org "mkdir /var/www/pylons-data/prod/PCAFiles/${ds_id}/${norm_type:0:len-4}  "

            scp /tmp/pca/${ds_id}/${ds_id}_${norm_type}_screetable.tsv isha@test.stemformatics.org:/var/www/pylons-data/prod/PCAFiles/${ds_id}/${norm_type:0:len-4}/screetable.tsv
            scp /tmp/pca/${ds_id}/${ds_id}_${norm_type}_pca.tsv isha@test.stemformatics.org:/var/www/pylons-data/prod/PCAFiles/${ds_id}/${norm_type:0:len-4}/pca.tsv

            scp /tmp/pca/${ds_id}/${ds_id}_metadata.tsv isha@test.stemformatics.org:/var/www/pylons-data/prod/PCAFiles/${ds_id}/${norm_type:0:len-4}/metadata.tsv
        done;
    done;


    #rm -fR /tmp/pca/*
fi
